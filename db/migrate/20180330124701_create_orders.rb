class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
    	t.belongs_to :product, index: true
        t.belongs_to :mobile_user, index: true
    	t.string :name, null: false
    	t.integer :quantity, null: false
    	t.integer :price, null: false
    	t.integer :table_number, null: false
    	t.string :mixers
    	t.timestamps
    end
  end
end
