module Api
  module V1
    class ProductDetailSerializer < ActiveModel::Serializer
      attributes :name, :price, :high_price, :base_price

      def name
      	object.name
      end

      def price
      	base = object.base_price
      	high = object.high_price
      	rand(base..high)
      end

      def high_price
      	object.high_price
      end


      def base_price
      	object.base_price
      end
    end
  end
end
