module Api
  module V1
    class AlcoholListSerializer < ActiveModel::Serializer
      attributes :categories

      def categories
        object.map { |category| Api::V1::ProductSerializer.new(category)}
      end
    end
  end
end
