module Api
  module V1
    class ProductSerializer < ActiveModel::Serializer
      attributes :title, :products

      def products
        object.products.map { |product| Api::V1::ProductDetailSerializer.new(product)}
      end

      def title
      	object.title
      end
    end
  end
end
