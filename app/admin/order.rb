ActiveAdmin.register Order do
  permit_params :name, :price, :table_number, :mixers, :quantity

  index do
    selectable_column
    id_column
    column :name
    column :quantity
    column :price
    column :table_number
    column :mixers
    column :is_paid
    actions
  end

  filter :name
  filter :quantity
  filter :price
  filter :mobile_user
  filter :table_number
  filter :is_paid

  form do |f|
    f.inputs do
      f.input :is_paid
    end
    f.actions
  end

end
